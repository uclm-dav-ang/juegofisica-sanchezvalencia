#ifndef Messi_H
#define Messi_H

#include <Ogre.h>
#include <cstdlib>
#include <btBulletDynamicsCommon.h>

#include "GameManager.h"

using namespace Ogre;
using namespace std;


class Messi{

private:
	SceneNode *_nodeMessi;
	btCollisionShape* _messiShape;
	btRigidBody* _messiRigidBody;
	GameManager *_gameManager;

	bool _hasImpacted;

public:
  Messi(SceneNode* nodeMessi, btCollisionShape* MessiShape, btRigidBody* MessiRigidBody);
  Messi(const Messi &obj);
  ~Messi();
  Messi& operator= (const Messi &obj);
  SceneNode* getNode() const;
  btCollisionShape* getMessiShape() const;
  btRigidBody* getMessiRigidBody() const;

  void hasImpacted();
  bool getHasImpacted();
};

#endif
