#ifndef Balon_H
#define Balon_H

#include <Ogre.h>
#include <cstdlib>
#include <btBulletDynamicsCommon.h>

#include "GameManager.h"

using namespace Ogre;
using namespace std;


class Balon{

private:
	SceneNode *_nodeBalon;
	btCollisionShape* _ballShape;
	btRigidBody* _ballRigidBody;
	GameManager *_gameManager;
	int _puntos;

public:
  Balon(SceneNode* nodeBalon, btCollisionShape* ballShape, btRigidBody* ballRigidBody);
  Balon(const Balon &obj);
  ~Balon();
  Balon& operator= (const Balon &obj);
  SceneNode* getNode() const;
  btCollisionShape* getBallShape() const;
  btRigidBody* getBallRigidBody() const;

  void disparar(btVector3 impulso);
  void pararBalon();
  void incrementarPuntos();
};

#endif
