#ifndef PlayStateNivel2_H
#define PlayStateNivel2_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <btBulletDynamicsCommon.h>

#include "PlayStateNivel3.h"

#define NUMERO_MESSIS_N2 6

class PlayStateNivel2 : public Ogre::Singleton<PlayStateNivel2>, public GameState
{
 public:
  PlayStateNivel2 () {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  // Heredados de Ogre::Singleton.
  static PlayStateNivel2& getSingleton ();
  static PlayStateNivel2* getSingletonPtr ();

  int getPuntuacion();
  void setPuntuacion(int puntuacion);
  void crearArbol(Vector3 traslacion, Vector3 escala);
  void crearMessi(double x, double y, double z);
  void crearPared(double x, double y, double z);

 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;

  bool _exitGame;

 private:
  GameManager *_gameManager;

  std::vector<char> _vector_nombre;
  char _nombre[20];

  Ogre::OverlayManager* _overlayManager;
  Ogre::AnimationState *_aimState;

  Ogre::Real _m_countDown;
  Ogre::Timer* _m_precisionTimer;

  btBroadphaseInterface* _broadphase;
  btDefaultCollisionConfiguration* _collisionConf;
  btCollisionDispatcher* _dispatcher;
  btSequentialImpulseConstraintSolver* _solver;
  btDiscreteDynamicsWorld* _world;

  btCollisionShape* _groundShape;
  btRigidBody* _groundRigidBody;


  btVector3 _impulse;

  bool _moveFront;
  bool _moveBack;
  bool _moveLeft;
  bool _moveRight;

  bool _turnUp;
  bool _turnDown;
  bool _turnLeft;
  bool _turnRight;

  bool _position1;
  double _fuerzaDisparo;
  bool _aumentarFuerza;
  bool _reducirFuerza;

  bool _disparar;

  Ogre::SceneNode* _node_principal;

  Ronaldo *_ronaldo;
  Flecha *_flecha;
  Balon *_balon;
  std::vector<Messi*> _vectorMessi;
  std::vector<SceneNode*> _vectorArbol;
  std::vector<SceneNode*> _vectorPared;

  Ogre::AnimationState *_animStateDisparar;
  Ogre::AnimationState *_animStateCelebrar;
  bool _celebrar;
  int _contadorFrames;

  int _puntuacion;
  int _siguienteNivel;
  int _contadorArboles;
  int _contadorMessis;
  int _contadorParedes;

  void CreateInitialWorld();
};

#endif
