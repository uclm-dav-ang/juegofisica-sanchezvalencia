#ifndef Ronaldo_H
#define Ronaldo_H

#include <Ogre.h>
#include <cstdlib>
#include <btBulletDynamicsCommon.h>

#include "GameManager.h"

using namespace Ogre;
using namespace std;


class Ronaldo{

private:
	SceneNode *_nodeRonaldo;
	GameManager *_gameManager;

public:
  Ronaldo(SceneNode* nodeRonaldo);
  Ronaldo(const Ronaldo &obj);
  ~Ronaldo();
  Ronaldo& operator= (const Ronaldo &obj);
  SceneNode* getNode() const;
};

#endif
