#include <Ogre.h>
#include <cstdlib>

#include "GameManager.h"
#include "Balon.h"

#define DISTANCIA_LIMITE 2
#define INICIO_X -5
#define INICIO_Z 0

using namespace Ogre;
using namespace std;


class Flecha{

private:
	SceneNode *_nodePadreFlecha;
	SceneNode *_nodeFlecha;
	SceneNode *_nodeFlechaLimit;
	GameManager *_gameManager;

	Balon *_balon;
	int _angulo;

	int _numDisparos;

public:
  Flecha(SceneNode* PadreFlecha, SceneNode* Flecha, SceneNode* nodeFlechaLimit, Balon* balon);
  Flecha(const Flecha &obj);
  ~Flecha();
  Flecha& operator= (const Flecha &obj);
  SceneNode* getNodePadreFlecha() const;
  SceneNode* getNodeFlecha() const;
  SceneNode* getNodeFlechaLimit() const;
  Balon* getBalon() const;
  void moveFront();
  void moveBack();
  void moveLeft();
  void moveRight();

  void turnUp();
  void turnDown();
  void turnLeft();
  void turnRight();
  void escalar(double escala);

  void disparar(btVector3 impulso);
  btVector3 calcularImpulso();
  int getDisparos();
  void decrementarDisparos();
  void ocultar();
  void mostrar();
  int getAngulo();
  void calcularAngulo();
};
