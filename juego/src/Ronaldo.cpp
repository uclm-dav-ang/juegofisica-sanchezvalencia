#include <Ronaldo.h>
#include <OgreSceneNode.h>
#include <cmath>

Ronaldo::Ronaldo(SceneNode* nodeRonaldo) {
	_nodeRonaldo = nodeRonaldo;
	_gameManager = GameManager::getSingletonPtr();
}

Ronaldo::Ronaldo(const Ronaldo &obj){
	_nodeRonaldo = obj.getNode();
}

Ronaldo::~Ronaldo() {
	delete _nodeRonaldo;
}

Ronaldo& Ronaldo::operator= (const Ronaldo &obj){
	delete _nodeRonaldo;
	_nodeRonaldo = obj._nodeRonaldo;
	return *this;
}

SceneNode* Ronaldo::getNode()const {
	return _nodeRonaldo;
}
