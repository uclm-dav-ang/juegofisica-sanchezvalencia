#include <Messi.h>
#include <OgreSceneNode.h>
#include <cmath>

Messi::Messi(SceneNode* nodeMessi, btCollisionShape* messiShape, btRigidBody* messiRigidBody) {
	_nodeMessi = nodeMessi;
	_messiShape = messiShape;
	_messiRigidBody = messiRigidBody;
	_gameManager = GameManager::getSingletonPtr();
	_hasImpacted = false;
}

Messi::Messi(const Messi &obj){
	_nodeMessi = obj.getNode();
	_messiShape = obj.getMessiShape();
	_messiRigidBody = obj.getMessiRigidBody();
}

Messi::~Messi() {
	delete _nodeMessi;
	delete _messiShape;
	delete _messiRigidBody;
}

Messi& Messi::operator= (const Messi &obj){
	delete _nodeMessi;
	delete _messiShape;
	delete _messiRigidBody;
	_nodeMessi = obj._nodeMessi;
	_messiShape = obj._messiShape;
	_messiRigidBody = obj._messiRigidBody;
	return *this;
}

SceneNode* Messi::getNode()const {
	return _nodeMessi;
}

btCollisionShape* Messi::getMessiShape()const {
	return _messiShape;
}

btRigidBody* Messi::getMessiRigidBody()const {
	return _messiRigidBody;
}

void Messi::hasImpacted(){
	_hasImpacted = true;
}

bool Messi::getHasImpacted(){
	return _hasImpacted;
}
