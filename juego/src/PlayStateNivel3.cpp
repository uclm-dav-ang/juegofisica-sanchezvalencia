#include "PlayStateNivel3.h"
#include "PlayState.h"
#include "PauseState.h"
#include "LoseState.h"
#include "IntroState.h"

using namespace Ogre;

template<> PlayStateNivel3* Ogre::Singleton<PlayStateNivel3>::msSingleton = 0;

void PlayStateNivel3::enter () {
	_gameManager = GameManager::getSingletonPtr();

	//desactivamos todos los movimientos
	_moveFront = false;
	_moveBack = false;
	_moveLeft = false;
	_moveRight = false;
	_turnUp = false;
	_turnDown = false;
	_turnLeft = false;
	_turnRight = false;

	_fuerzaDisparo = 1;

	_puntuacion = PlayStateNivel2::getSingletonPtr()->getPuntuacion();
	_vectorMessi.clear();
	_vectorArbol.clear();
	_vectorPared.clear();

	_siguienteNivel = 0;

	_disparar = false;
	_contadorFrames = 0;

	_impulse = btVector3(0,0,0);

	_m_countDown = TIEMPO_NIVEL3;
	_m_precisionTimer = new Ogre::Timer();

	_position1 = false;
	_root = Ogre::Root::getSingletonPtr();

	// Se recupera el gestor de escena y la cámara.
	_sceneMgr = _root->getSceneManager("SceneManager");
	_sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_TEXTURE_MODULATIVE);

	_camera = _sceneMgr->getCamera("IntroCamera");
	_camera->setPosition(Ogre::Vector3(-20, 25, -20));
	_camera->lookAt(Ogre::Vector3(10, 0, 0));
	_camera->setNearClipDistance(5);
	_camera->setFarClipDistance(100);
	_camera->setFOVy(Ogre::Degree(50));
	_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
	_viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.1, 1.0));

	//creamos un punto de luz
	Light* light = _sceneMgr->createLight("Light");
	light->setPosition(-15, 30, 0);
	light->setDirection(-50, -1, 0);
	light->setSpotlightInnerAngle(Degree(5.0f));
	light->setSpotlightOuterAngle(Degree(90.0f));
	light->setDiffuseColour(-1.0, -0.5, -0.2);
	light->setSpecularColour(-1.0, -0.5, -0.2);
	light->setType(Light::LT_POINT);
	light->setSpotlightFalloff(5.0f);
	light->setCastShadows(true);

	//Creamos el nodo principal del que colgarán los demás
	_node_principal = _sceneMgr->createSceneNode("principal");
	_sceneMgr->getRootSceneNode()->addChild(_node_principal);


	_broadphase = new btDbvtBroadphase();
	_collisionConf = new btDefaultCollisionConfiguration();
	_dispatcher = new btCollisionDispatcher(_collisionConf);
	_solver = new btSequentialImpulseConstraintSolver;
	_world = new btDiscreteDynamicsWorld(_dispatcher, _broadphase, _solver,_collisionConf);

	// Establecimiento propiedades del mundo
	_world->setGravity(btVector3(0,-10,0));

	// Creacion de los elementos iniciales del mundo
	CreateInitialWorld();

	//asignamos la animacion de CR7
	_animStateDisparar = _sceneMgr->getEntity("Ronaldo")->getAnimationState("Disparar");
	_animStateCelebrar = _sceneMgr->getEntity("Ronaldo")->getAnimationState("Celebrar");
	_animStateDisparar->setEnabled(false);
	_animStateCelebrar->setEnabled(false);

	_exitGame = false;

}

void PlayStateNivel3::exit () {
	if(_sceneMgr->hasEntity("PadreFlecha")) _sceneMgr->destroyEntity("PadreFlecha");
	if(_sceneMgr->hasEntity("Flecha")) _sceneMgr->destroyEntity("Flecha");
	if(_sceneMgr->hasEntity("Balon")) _sceneMgr->destroyEntity("Balon");
	if(_sceneMgr->hasEntity("Ronaldo")) _sceneMgr->destroyEntity("Ronaldo");
	if(_sceneMgr->hasEntity("planeEnt")) _sceneMgr->destroyEntity("planeEnt");
	if(_sceneMgr->hasEntity("Obstaculo")) _sceneMgr->destroyEntity("Obstaculo");
	if(_sceneMgr->hasSceneNode("PadreFlecha")) _sceneMgr->destroySceneNode("PadreFlecha");
	if(_sceneMgr->hasSceneNode("Flecha")) _sceneMgr->destroySceneNode("Flecha");
	if(_sceneMgr->hasSceneNode("FlechaLimit")) _sceneMgr->destroySceneNode("FlechaLimit");
	if(_sceneMgr->hasSceneNode("Balon")) _sceneMgr->destroySceneNode("Balon");
	if(_sceneMgr->hasSceneNode("Ronaldo")) _sceneMgr->destroySceneNode("Ronaldo");
	if(_sceneMgr->hasSceneNode("principal")) _sceneMgr->destroySceneNode("principal");
	if(_sceneMgr->hasSceneNode("ground")) _sceneMgr->destroySceneNode("ground");
	if(_sceneMgr->hasSceneNode("Obstaculo")) _sceneMgr->destroySceneNode("Obstaculo");
	if(_sceneMgr->hasLight("Light")) _sceneMgr->destroyLight("Light");


	int i;
	for(i = 0; i < (int)_vectorArbol.size(); i++){
		if(_sceneMgr->hasEntity(_vectorArbol[i]->getName())) _sceneMgr->destroyEntity(_vectorArbol[i]->getName());
		if(_sceneMgr->hasSceneNode(_vectorArbol[i]->getName())) _sceneMgr->destroySceneNode(_vectorArbol[i]);
	}

	int j;
	for(j = 0; j < (int)_vectorMessi.size(); j++){
		if(_sceneMgr->hasEntity(_vectorMessi[j]->getNode()->getName())) _sceneMgr->destroyEntity(_vectorMessi[j]->getNode()->getName());
		if(_sceneMgr->hasSceneNode(_vectorMessi[j]->getNode()->getName())) _sceneMgr->destroySceneNode(_vectorMessi[j]->getNode());

		int k;
		for(k = 0; k < (int)_vectorPared.size(); k++){
			if(_sceneMgr->hasEntity(_vectorPared[k]->getName())) _sceneMgr->destroyEntity(_vectorPared[k]->getName());
			if(_sceneMgr->hasSceneNode(_vectorPared[k]->getName())) _sceneMgr->destroySceneNode(_vectorPared[k]);
		}
	}

  Ogre::OverlayManager::getSingletonPtr()->getByName("Info")->hide();
  Ogre::OverlayManager::getSingletonPtr()->getByName("NumDisparos")->hide();
  Ogre::OverlayManager::getSingletonPtr()->getByName("Angulo")->hide();

  _root->getAutoCreatedWindow()->removeAllViewports();
}

void PlayStateNivel3::pause() {
	_moveFront = false;
	_moveBack = false;
	_moveLeft = false;
	_moveRight = false;

	_turnUp = false;
	_turnDown = false;
	_turnLeft = false;
	_turnRight = false;
}

void PlayStateNivel3::resume() {
  // Se restaura el background colour.
  _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.1, 1.0));
  _camera->setPosition(Ogre::Vector3(-20, 25, -20));
  _camera->lookAt(Ogre::Vector3(10, 0, 0));
}

bool PlayStateNivel3::frameStarted (const Ogre::FrameEvent& evt) {
	Ogre::Real deltaT = evt.timeSinceLastFrame;

	_world->stepSimulation(deltaT, 1); // Actualizar simulacion Bullet

	if(_moveFront){
		_flecha->moveFront();
	}

	if(_moveBack){
		_flecha->moveBack();
	}

	if(_moveLeft){
		_flecha->moveLeft();
	}

	if(_moveRight){
		_flecha->moveRight();
	}
	if(_turnUp){
		_flecha->turnUp();
	}

	if(_turnDown){
		_flecha->turnDown();
	}
	if(_turnLeft){
		_flecha->turnLeft();
	}
	if(_turnRight){
		_flecha->turnRight();
	}

	//Escala de la flecha y fuerza del disparo mientras se pulsa el espacio
	if(_fuerzaDisparo < 3 && _aumentarFuerza){
		_fuerzaDisparo += 0.05;
		_flecha->escalar(_fuerzaDisparo);
	} else if (_fuerzaDisparo >= 3 && _aumentarFuerza){
		_aumentarFuerza = false;
		_reducirFuerza = true;
	}
	if(_fuerzaDisparo > 1 && _reducirFuerza){
		_fuerzaDisparo -= 0.05;
		_flecha->escalar(_fuerzaDisparo);
	} else if (_fuerzaDisparo <= 1 && _reducirFuerza){
		_aumentarFuerza = true;
		_reducirFuerza = false;
	}

	Ogre::Real deltaTime = _m_precisionTimer->getMilliseconds();
	_m_precisionTimer->reset();

	_m_countDown -= deltaTime; //descontamos un segundo


	for (int i = 0; i < (int)_vectorMessi.size(); i++){
		if (!_vectorMessi[i]->getHasImpacted()){
			if(_vectorMessi[i]->getNode()->getPosition().y < 3){
				_siguienteNivel++;
				_vectorMessi[i]->hasImpacted();
				_puntuacion += 10;
				//activamos la celebración
				_celebrar = true;
			}
		}
	}

	//se muestra la flecha
	if (!_animStateDisparar->getEnabled() && !_animStateCelebrar->getEnabled() && _position1) {
		_flecha->mostrar();
	}

	//se oculta la flecha si alguna animacion está activa
	if (_animStateDisparar->getEnabled() || _animStateCelebrar->getEnabled() || !_position1) {
		_flecha->ocultar();
	}

	//animacion de disparo
	if (_animStateDisparar->getEnabled() && !_animStateDisparar->hasEnded()){
	    _animStateDisparar->addTime(deltaT);
	    _contadorFrames++;
	}
	if(_animStateDisparar->hasEnded()){
		_animStateDisparar->setEnabled(false);
		_animStateDisparar->setLoop(false);
	}

	//animacion de la celebracion
	if (_celebrar && _animStateDisparar->hasEnded()){
		_animStateCelebrar->setTimePosition(0.0);
		_animStateCelebrar->setEnabled(true);
		_animStateCelebrar->setLoop(false);
		_celebrar = false;

		_gameManager->getSoundFXPtr()->play();
	}
	if (_animStateCelebrar->getEnabled() && !_animStateCelebrar->hasEnded()){
		_animStateCelebrar->addTime(deltaT);
		_contadorFrames++;
	}
	if(_animStateCelebrar->hasEnded()){
		_animStateCelebrar->setEnabled(false);
		_animStateCelebrar->setLoop(false);
	}

	//se efectua el disparo
	if(_contadorFrames == FRAME_DE_DISPARO && _disparar){
		_balon->getNode()->setVisible(true);
		_balon->getBallRigidBody()->activate(true);
		_balon->getBallRigidBody()->setWorldTransform(btTransform(btQuaternion(1,0,0,0),btVector3(_ronaldo->getNode()->_getDerivedPosition().x,_ronaldo->getNode()->_getDerivedPosition().y+0.16,_ronaldo->getNode()->_getDerivedPosition().z)));

		//calculamos la direccion del impulso
		_impulse = _flecha->calcularImpulso();
		_flecha->disparar(_impulse);
		_flecha->escalar(1);
		_flecha->decrementarDisparos();
		_disparar = false;

		_gameManager->getSoundGolpeo()->play();
	}

	//ganamos la partida y pasamos al estado final
	if(_siguienteNivel == NUMERO_MESSIS_N3){
		if(!_animStateDisparar->getEnabled() && !_animStateCelebrar->getEnabled()){
			_puntuacion = _puntuacion + (int)(_m_countDown/1000);
			Ogre::OverlayManager::getSingletonPtr()->getByName("NumDisparos")->hide();
			Ogre::OverlayManager::getSingletonPtr()->getByName("Info")->hide();
			Ogre::OverlayManager::getSingletonPtr()->getByName("Angulo")->hide();
			PlayState::getSingletonPtr()->setPuntuacion(_puntuacion);
			changeState(LoseState::getSingletonPtr());
		}
	}

	//perdemos por llegar al limite de tiempo
	if(_m_countDown < 0){
		_m_countDown = 0;
		PlayState::getSingletonPtr()->setPuntuacion(_puntuacion);
		changeState(LoseState::getSingletonPtr());
	}

	//perdemos al agotar los disparos
	if(_flecha->getDisparos() == 0  && !_animStateDisparar->getEnabled() && !_animStateCelebrar->getEnabled() && _siguienteNivel < NUMERO_MESSIS_N3){
		PlayState::getSingletonPtr()->setPuntuacion(_puntuacion);
		changeState(LoseState::getSingletonPtr());
	}

	_overlayManager = Ogre::OverlayManager::getSingletonPtr();
	Ogre::Overlay *overlayInfo = _overlayManager->getByName("Info");
	Ogre::Overlay *overlayDisparos = _overlayManager->getByName("NumDisparos");
	Ogre::Overlay *overlayAngulo = _overlayManager->getByName("Angulo");
	overlayInfo->show();
	overlayDisparos->show();
	overlayAngulo->show();

	Ogre::OverlayElement *oe;
	oe = _overlayManager->getOverlayElement("Puntos");
	oe->setCaption(Ogre::StringConverter::toString(_puntuacion));
	oe = _overlayManager->getOverlayElement("Vidas");
	oe->setCaption(Ogre::StringConverter::toString((int)_m_countDown/1000));

	oe = _overlayManager->getOverlayElement("Disparos");
	oe->setCaption(Ogre::StringConverter::toString(_flecha->getDisparos()));

	oe = _overlayManager->getOverlayElement("ang");
	if(_siguienteNivel < NUMERO_MESSIS_N3 && _flecha->getDisparos() > 0 && _m_countDown > 0) oe->setCaption(Ogre::StringConverter::toString(_flecha->getAngulo()));

  return true;
}

bool PlayStateNivel3::frameEnded (const Ogre::FrameEvent& evt) {
  if (_exitGame)
    return false;
  
  return true;
}

void PlayStateNivel3::keyPressed (const OIS::KeyEvent &e) {
  // Tecla p --> PauseState.
  if (e.key == OIS::KC_P) {
	  pushState(PauseState::getSingletonPtr());
  }
  if (e.key == OIS::KC_DOWN) {
	  if(!_position1){
		  _moveBack = true;
	  }else{
		  _turnDown = true;
	  }
  }
  if (e.key == OIS::KC_UP) {
	  if(!_position1){
	  	  _moveFront = true;
	  }else{
		  _turnUp = true;
	  }
  }
  if (e.key == OIS::KC_LEFT) {
	  if(!_position1){
		  _moveLeft = true;
	  }else{
		  _turnLeft = true;
	  }
  }
  if (e.key == OIS::KC_RIGHT) {
	  if(!_position1){
		  _moveRight= true;
	  }else{
		  _turnRight = true;
	  }
  }
  if (e.key == 28) {
	  _position1 = true;
	  _moveFront = false;
	  _moveBack = false;
	  _moveLeft = false;
	  _moveRight = false;
  }
  if (e.key == OIS::KC_ESCAPE) {
	  _position1 = false;
	  _turnUp = false;
	  _turnDown = false;
	  _turnLeft = false;
	  _turnRight = false;
  }
  if (e.key == OIS::KC_SPACE && !_animStateDisparar->getEnabled() && !_animStateCelebrar->getEnabled() && _position1) {
	  _aumentarFuerza = true;
  }
}

void PlayStateNivel3::keyReleased (const OIS::KeyEvent &e) {
	if (e.key == OIS::KC_DOWN) {
		if(!_position1){
			_moveBack = false;
		}else{
			_turnDown = false;
		}
	}

	if (e.key == OIS::KC_UP) {
		if(!_position1){
			_moveFront = false;
		}else{
			_turnUp = false;
		}
	}

	if (e.key == OIS::KC_LEFT) {
		if(!_position1){
			_moveLeft = false;
		}else{
			_turnLeft = false;
		}
	}

	if (e.key == OIS::KC_RIGHT) {
		if(!_position1){
			_moveRight = false;
		}else{
			_turnRight = false;
		}
	}

	if (e.key == OIS::KC_SPACE && !_animStateDisparar->getEnabled() && !_animStateCelebrar->getEnabled() && _position1) {
		_animStateDisparar->setTimePosition(0.0);
		_animStateDisparar->setEnabled(true);
		_animStateDisparar->setLoop(false);
		_disparar = true;
		_contadorFrames = 0;
		_aumentarFuerza = false;
		_reducirFuerza = false;
		_fuerzaDisparo = 1;
		_flecha->ocultar();
	}
}

void PlayStateNivel3::mouseMoved (const OIS::MouseEvent &e) {
}

void PlayStateNivel3::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
}

void PlayStateNivel3::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
}

PlayStateNivel3* PlayStateNivel3::getSingletonPtr () {
return msSingleton;
}

PlayStateNivel3& PlayStateNivel3::getSingleton () {
  assert(msSingleton);
  return *msSingleton;
}

void PlayStateNivel3::CreateInitialWorld() {
	// Creacion de la entidad y del SceneNode ------------------------
	Plane plane1(Vector3(0,1,0), 0);    // Normal y distancia
	MeshManager::getSingleton().createPlane("p1",
	ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane1,
	200, 200, 1, 1, true, 1, 20, 20, Vector3::UNIT_Z);
	SceneNode* node = _sceneMgr->createSceneNode("ground");
	Entity* groundEnt = _sceneMgr->createEntity("planeEnt", "p1");
	groundEnt->setMaterialName("Ground");
	node->attachObject(groundEnt);
	groundEnt->setCastShadows(false);
	_sceneMgr->getRootSceneNode()->addChild(node);

	// Creamos las formas de colision ---------------------------------
	_groundShape = new btStaticPlaneShape(btVector3(0,1,0),1);
	btCollisionShape* ballShape = new btSphereShape(0.14);
	btCollisionShape* ObstaculoShape = new btBoxShape(btVector3(0.5,4.5,9));

	// Creamos el plano -----------------------------------------------
	MyMotionState* groundMotionState = new MyMotionState(
	btTransform(btQuaternion(0,0,0,1),btVector3(0,-1,0)), node);
	btRigidBody::btRigidBodyConstructionInfo
	groundRigidBodyCI(0,groundMotionState,_groundShape,btVector3(0,0,0));
	_groundRigidBody = new btRigidBody(groundRigidBodyCI);
	_groundRigidBody->setRollingFriction(1);
	_groundRigidBody->setRollingFriction(1);
	_world->addRigidBody(_groundRigidBody);

	//creamos un nodo invisible como padre de la flecha
	SceneNode* nodePadreFlecha = _sceneMgr->createSceneNode("PadreFlecha");
	_node_principal->addChild(nodePadreFlecha);
	nodePadreFlecha->translate(-5, 0.25, 0);

	//Creamos el nodo para la flecha
	SceneNode* nodeFlecha = _sceneMgr->createSceneNode("Flecha");
	Ogre::Entity* entFlecha = _sceneMgr->createEntity("Flecha", "Flecha.mesh");
	nodeFlecha->attachObject(entFlecha);
	nodePadreFlecha->addChild(nodeFlecha);

	//Creamos el nodo que servira como el borde delantero de la flecha
	SceneNode* nodeFlechaLimit = _sceneMgr->createSceneNode("FlechaLimit");
	nodeFlecha->addChild(nodeFlechaLimit);
	nodeFlechaLimit->translate(LARGO_FLECHA, 0, 0);

	//Creamos los arboles
	crearArbol(Vector3(-5,0,10), Vector3(2,2,2));
	crearArbol(Vector3(10,0,31), Vector3(1.3,1.3,1.3));
	crearArbol(Vector3(15,0,31), Vector3(1.6,1.6,1.6));
	crearArbol(Vector3(35,0,0), Vector3(1,1,1));
	crearArbol(Vector3(40,0,28), Vector3(2,2,2));
	crearArbol(Vector3(40,0,10), Vector3(1,1,1));
	crearArbol(Vector3(32,0,-3), Vector3(1,1,1));
	crearArbol(Vector3(50,0,-15), Vector3(2,2,2));
	crearArbol(Vector3(16,0,20), Vector3(1.3,1.3,1.3));
	crearArbol(Vector3(0,0,15), Vector3(0.8,0.8,0.8));
	crearArbol(Vector3(55,0,15), Vector3(1.5,1.5,1.5));
	crearArbol(Vector3(50,0,-0), Vector3(1.2,1.2,1.2));
	crearArbol(Vector3(45,0,-15), Vector3(1.6,1.6,1.6));


	//Creamos el nodo para el balon
	SceneNode* node_balon = _sceneMgr->createSceneNode("Balon");
	Ogre::Entity* entBalon = _sceneMgr->createEntity("Balon", "Balon.mesh");
	node_balon->attachObject(entBalon);
	_sceneMgr->getRootSceneNode()->addChild(node_balon);
	node_balon->setVisible(false);
	MyMotionState* fallMotionState = new MyMotionState(
	btTransform(btQuaternion(0,0,0,1),btVector3(-5,0,0)), node_balon);
	btScalar ballMass = 1;
	btVector3 fallInertia(0,0,0);
	ballShape->calculateLocalInertia(ballMass,fallInertia);
	btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI(
	ballMass,fallMotionState,ballShape,fallInertia);
	btRigidBody* ballRigidBody = new btRigidBody(fallRigidBodyCI);
	ballRigidBody->setFriction(1);
	ballRigidBody->setRollingFriction(1);
	_world->addRigidBody(ballRigidBody);
	_balon = new Balon(node_balon, ballShape, ballRigidBody);
	_flecha = new Flecha(nodePadreFlecha, nodeFlecha, nodeFlechaLimit, _balon);
	_flecha->ocultar();

	//Creamos los messis
	crearMessi(20, 5.5, 0);
	crearMessi(20, 5.5, 12);
	crearMessi(20, 5.5, -12);

	//Creamos a CR7
	SceneNode* nodeRonaldo = _sceneMgr->createSceneNode("Ronaldo");
	Ogre::Entity* entRonaldo = _sceneMgr->createEntity("Ronaldo", "basic_footballer.mesh");
	nodeRonaldo->attachObject(entRonaldo);
	nodePadreFlecha->addChild(nodeRonaldo);
	nodeRonaldo->translate(-0.5, -0.25, 0);
	nodeRonaldo->yaw(Degree(90));
	_ronaldo = new Ronaldo(nodeRonaldo);

	//Creamos las paredes
	crearPared(20, 1.5, 0);
	crearPared(20, 1.5, 12);
	crearPared(20, 1.5, -12);

	//Creamos los muros
	SceneNode* nodeObstaculo = _sceneMgr->createSceneNode("Obstaculo");
	Ogre::Entity* entObstaculo = _sceneMgr->createEntity("Obstaculo", "Obstaculo.mesh");
	nodeObstaculo->setScale(1,1,2);
	nodeObstaculo->attachObject(entObstaculo);
	_sceneMgr->getRootSceneNode()->addChild(nodeObstaculo);
	MyMotionState* MotionStateObstaculo = new MyMotionState(
				btTransform(btQuaternion(0,0,0,1),btVector3(5,4.5,0)), nodeObstaculo);
	ObstaculoShape->calculateLocalInertia(1000,fallInertia);
	btRigidBody::btRigidBodyConstructionInfo ObstaculoRigidBodyCI(
				  1000,MotionStateObstaculo,ObstaculoShape,fallInertia);
	btRigidBody* ObstaculoRigidBody = new btRigidBody(ObstaculoRigidBodyCI);
	ObstaculoRigidBody->setFriction(1);
	_world->addRigidBody(ObstaculoRigidBody);

}

int PlayStateNivel3::getPuntuacion(){
	return _puntuacion;
}

void PlayStateNivel3::setPuntuacion(int puntuacion){
	_puntuacion = puntuacion;
}

void PlayStateNivel3::crearArbol(Vector3 traslacion, Vector3 escala) {
	std::stringstream saux;
	std::string s = "Tree";

	saux << s << _contadorArboles;
	_contadorArboles++;

	//Creamos el nodo para el arbol
	SceneNode* nodeTree = _sceneMgr->createSceneNode(saux.str());
	Ogre::Entity* entTree = _sceneMgr->createEntity(saux.str(), "Tree.mesh");
	nodeTree->attachObject(entTree);
	_node_principal->addChild(nodeTree);
	nodeTree->translate(traslacion);
	nodeTree->setScale(escala);
	_vectorArbol.push_back(nodeTree);

	saux.str("");
}

void PlayStateNivel3::crearMessi(double x, double y, double z) {
	btCollisionShape* messiShape = new btBoxShape(btVector3(0.25,2.5,1));
	btVector3 fallInertia(0,0,0);

	std::stringstream saux;
	std::string s = "Messi";

	saux << s << _contadorMessis;
	_contadorMessis++;

	//Creamos el nodo para messi
	SceneNode* nodeMessi = _sceneMgr->createSceneNode(saux.str());
	Ogre::Entity* entMessi = _sceneMgr->createEntity(saux.str(), "Messi.mesh");
	nodeMessi->attachObject(entMessi);
	_sceneMgr->getRootSceneNode()->addChild(nodeMessi);
	MyMotionState* messiMotionState = new MyMotionState(
	         btTransform(btQuaternion(0,0,0,1),btVector3(x,y,z)), nodeMessi);
	btScalar messiMass = 2;
	messiShape->calculateLocalInertia(messiMass,fallInertia);
	btRigidBody::btRigidBodyConstructionInfo messiRigidBodyCI(
	  		  messiMass,messiMotionState,messiShape,fallInertia);
	btRigidBody* messiRigidBody = new btRigidBody(messiRigidBodyCI);
	messiRigidBody->setFriction(1);
	_world->addRigidBody(messiRigidBody);
	Messi *messi = new Messi(nodeMessi, messiShape, messiRigidBody);
	_vectorMessi.push_back(messi);

	saux.str("");
}

void PlayStateNivel3::crearPared(double x, double y, double z) {
	btCollisionShape* wallShape = new btBoxShape(btVector3(0.5,1.5,1.5));
	btVector3 fallInertia(0,0,0);

	std::stringstream saux;
	std::string s = "Wall";

	saux << s << _contadorParedes;
	_contadorParedes++;

	SceneNode* nodeWall = _sceneMgr->createSceneNode(saux.str());
	Ogre::Entity* entWall = _sceneMgr->createEntity(saux.str(), "Wall.mesh");
	nodeWall->attachObject(entWall);
	_sceneMgr->getRootSceneNode()->addChild(nodeWall);
	MyMotionState* MotionStateWall = new MyMotionState(
				btTransform(btQuaternion(0,0,0,1),btVector3(x,y,z)), nodeWall);
	wallShape->calculateLocalInertia(1000,fallInertia);
	btRigidBody::btRigidBodyConstructionInfo WallRigidBodyCI(
				  1000,MotionStateWall,wallShape,fallInertia);
	btRigidBody* WallRigidBody = new btRigidBody(WallRigidBodyCI);
	WallRigidBody->setFriction(1);
	_world->addRigidBody(WallRigidBody);
	_vectorPared.push_back(nodeWall);
}
