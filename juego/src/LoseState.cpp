#include "LoseState.h"
#include "IntroState.h"
#include "PlayState.h"

template<> LoseState* Ogre::Singleton<LoseState>::msSingleton = 0;

void LoseState::enter() {
	_contadorNombre = 0;
	int i;
	//se vacía el nombre anterior
	for (i = 0; i < int((sizeof(_nombre)/sizeof(_nombre[0]))); i++){
		_nombre[i] = 0;
	}

	_root = Ogre::Root::getSingletonPtr();

	// Se recupera el gestor de escena y la cámara.
	_sceneMgr = _root->getSceneManager("SceneManager");
	_camera = _sceneMgr->getCamera("IntroCamera");
	_camera->setPosition(Ogre::Vector3(0.01, 20, 0));
	_camera->lookAt(Ogre::Vector3(0, 0, 0));
	_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

	crearMenu();

	Ogre::OverlayManager::getSingletonPtr()->getByName("Info")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("Angulo")->hide();

	_exitGame = false;
}

void LoseState::exit() {
	_root->getAutoCreatedWindow()->removeAllViewports();
	_sceneMgr->destroyAllCameras();
	_sceneMgr->clearScene();
	_root->destroySceneManager(_sceneMgr);

	almacenarPuntuacion();
	Ogre::OverlayManager::getSingletonPtr()->getByName("Nombre")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("NumDisparos")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("Info")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("Angulo")->hide();
}

void LoseState::pause() {
}

void LoseState::resume() {
}

bool LoseState::frameStarted(const Ogre::FrameEvent& evt) {
	_overlayManager = Ogre::OverlayManager::getSingletonPtr();
	Ogre::Overlay *overlay = _overlayManager->getByName("Nombre");
	overlay->show();

	Ogre::OverlayElement *oe;
	oe = _overlayManager->getOverlayElement("NombreTeclado");
	oe->setCaption(_nombre);
	return true;
}

bool LoseState::frameEnded(const Ogre::FrameEvent& evt) {
	if (_exitGame)
		return false;

	return true;
}

void LoseState::keyPressed(const OIS::KeyEvent &e) {
	// Tecla p --> Estado anterior.
	if (e.key == 28 && _nombre[0]) {
		changeState(IntroState::getSingletonPtr());
	}

	//se rellena el nombre
	if (((16 <= e.key && e.key <= 25) || (30 <= e.key && e.key <= 38) || (44 <= e.key && e.key <= 50)) && (_contadorNombre < 3)){
		char letra = int(e.text);
		_nombre[_contadorNombre] = letra;
		_contadorNombre++;
	}

	if (e.key == OIS::KC_BACK && _contadorNombre > 0){
		_contadorNombre--;
		_nombre[_contadorNombre] = 0;
	}
}

void LoseState::almacenarPuntuacion(){
	char *fichero = "score.txt";

	std::fstream fs;
	fs.open(fichero, std::fstream::in | std::fstream::out | std::fstream::ate);
	fs << _nombre << ": " << PlayState::getSingletonPtr()->getPuntuacion() << endl;
	fs.close();
}

void LoseState::keyReleased(const OIS::KeyEvent &e) {
}

void LoseState::mouseMoved(const OIS::MouseEvent &e) {
}

void LoseState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
}

void LoseState::mouseReleased(const OIS::MouseEvent &e,
		OIS::MouseButtonID id) {
}

void LoseState::crearMenu(){
	//fondo
	Ogre::SceneNode *node_menu = _sceneMgr->createSceneNode("Menu");
	_sceneMgr->getRootSceneNode()->addChild(node_menu);

	//Nodo PlanoMenu
	Ogre::SceneNode *node_PlanoMenu = _sceneMgr->createSceneNode("PlanoMenu");
	Ogre::Entity* ent_PlanoMenu = _sceneMgr->createEntity("PlanoMenu", "PlanoMenu.mesh");
	node_PlanoMenu->attachObject(ent_PlanoMenu);
	node_menu->addChild(node_PlanoMenu);
	node_PlanoMenu->translate(0, 12, 0);
	node_PlanoMenu->yaw(Degree(90));
}

LoseState*
LoseState::getSingletonPtr() {
	return msSingleton;
}

LoseState&
LoseState::getSingleton() {
	assert(msSingleton);
	return *msSingleton;
}
