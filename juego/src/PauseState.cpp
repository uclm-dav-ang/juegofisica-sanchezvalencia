#include "PauseState.h"
#include "IntroState.h"
#include "PlayState.h"

template<> PauseState* Ogre::Singleton<PauseState>::msSingleton = 0;

void PauseState::enter () {
  _root = Ogre::Root::getSingletonPtr();

  // Se recupera el gestor de escena y la cámara.
  _sceneMgr = _root->getSceneManager("SceneManager");
  _camera = _sceneMgr->getCamera("IntroCamera");
  _camera->setPosition(Ogre::Vector3(11.2, 9, 0));
  _camera->lookAt(Ogre::Vector3(3, 0, 0));
  _viewport = _root->getAutoCreatedWindow()->getViewport(0);

  _menuSeleccion = 0;
  crearMenu();
  cambiarTexturaMenu();

  _exitGame = false;
}

void PauseState::exit () {
}

void PauseState::pause () {
}

void PauseState::resume () {
}

bool PauseState::frameStarted (const Ogre::FrameEvent& evt) {
  return true;
}

bool PauseState::frameEnded (const Ogre::FrameEvent& evt) {
  if (_exitGame)
    return false;
  
  return true;
}

void PauseState::keyPressed (const OIS::KeyEvent &e) {
  if (e.key == 28 && _menuSeleccion == 0) {
	  _sceneMgr->destroyEntity("Continuar");
	  _sceneMgr->destroySceneNode("Continuar");
	  _sceneMgr->destroyEntity("Salir");
	  _sceneMgr->destroySceneNode("Salir");
	  _sceneMgr->destroySceneNode("Menu");
	  _sceneMgr->destroyEntity("PlanoMenu");
	  _sceneMgr->destroySceneNode("PlanoMenu");
	  popState();
  }

  if (e.key == 28 && _menuSeleccion == 1) {
	  _root->getAutoCreatedWindow()->removeAllViewports();
	  _sceneMgr->destroyAllCameras();
	  _sceneMgr->clearScene();
	  _root->destroySceneManager(_sceneMgr);

	  Ogre::OverlayManager::getSingletonPtr()->getByName("Info")->hide();
	  Ogre::OverlayManager::getSingletonPtr()->getByName("NumDisparos")->hide();
	  Ogre::OverlayManager::getSingletonPtr()->getByName("Angulo")->hide();
	  changeState(IntroState::getSingletonPtr());
  }

  if (e.key == OIS::KC_UP && _menuSeleccion > 0){
	  _menuSeleccion--;
	  cambiarTexturaMenu();
  	 }

  if (e.key == OIS::KC_DOWN && _menuSeleccion < 1){
	  _menuSeleccion++;
	  cambiarTexturaMenu();
  }
}

void PauseState::crearMenu(){
	Ogre::SceneNode *node_menu = _sceneMgr->createSceneNode("Menu");
	_sceneMgr->getRootSceneNode()->addChild(node_menu);

	//Nodo PlanoMenu
	Ogre::SceneNode *node_PlanoMenu = _sceneMgr->createSceneNode("PlanoMenu");
	Ogre::Entity* ent_PlanoMenu = _sceneMgr->createEntity("PlanoMenu", "PlanoMenu.mesh");
	node_PlanoMenu->attachObject(ent_PlanoMenu);
	node_menu->addChild(node_PlanoMenu);
	node_PlanoMenu->translate(0, -10, 0);

	//Nodo Continuar
	Ogre::SceneNode *node_continuar = _sceneMgr->createSceneNode("Continuar");
	Ogre::Entity* ent_continuar = _sceneMgr->createEntity("Continuar", "Plane.mesh");
	node_continuar->attachObject(ent_continuar);
	node_menu->addChild(node_continuar);
	node_continuar->translate(-2.5, 0, 0);
	node_continuar->yaw(Ogre::Degree(90));
	node_continuar->pitch(Ogre::Degree(90));
	ent_continuar->setMaterialName("MaterialMenuM5");

	//Nodo Salir
	Ogre::SceneNode *node_salir = _sceneMgr->createSceneNode("Salir");
	Ogre::Entity* ent_salir = _sceneMgr->createEntity("Salir", "Plane.mesh");
	node_salir->attachObject(ent_salir);
	node_menu->addChild(node_salir);
	node_salir->translate(2.5, 0, 0);
	node_salir->yaw(Ogre::Degree(90));
	node_salir->pitch(Ogre::Degree(90));
	node_salir->scale(0.8, 1, 0.8);
	ent_salir->setMaterialName("MaterialMenuN4");
}



void PauseState::cambiarTexturaMenu(){
	switch (_menuSeleccion){
	case 0:
		_sceneMgr->getEntity("Continuar")->setMaterialName("MaterialMenuM5");
		_sceneMgr->getEntity("Salir")->setMaterialName("MaterialMenuN4");
		break;
	case 1:
		_sceneMgr->getEntity("Salir")->setMaterialName("MaterialMenuM4");
		_sceneMgr->getEntity("Continuar")->setMaterialName("MaterialMenuN5");
		break;
	}
}

void PauseState::keyReleased (const OIS::KeyEvent &e) {
}

void PauseState::mouseMoved (const OIS::MouseEvent &e) {
}

void PauseState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
}

void PauseState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
}

PauseState* PauseState::getSingletonPtr () {
return msSingleton;
}

PauseState& PauseState::getSingleton () {
  assert(msSingleton);
  return *msSingleton;
}
