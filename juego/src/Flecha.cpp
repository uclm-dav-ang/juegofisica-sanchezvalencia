#include "Flecha.h"
#include <OgreSceneNode.h>
#include <cmath>

Flecha::Flecha(SceneNode* nodePadreFlecha, SceneNode* nodeFlecha, SceneNode* nodeFlechaLimit, Balon* balon) {
	_nodePadreFlecha = nodePadreFlecha;
	_nodeFlecha = nodeFlecha;
	_nodeFlechaLimit = nodeFlechaLimit;
	_gameManager = GameManager::getSingletonPtr();
	_balon = balon;
	_numDisparos = 10;
}

Flecha::Flecha(const Flecha &obj){
	_nodePadreFlecha = obj.getNodePadreFlecha();
	_nodeFlecha = obj.getNodeFlecha();
	_nodeFlechaLimit = obj.getNodeFlechaLimit();
	_balon = obj.getBalon();
}

Flecha::~Flecha() {
	delete _nodePadreFlecha;
	delete _nodeFlecha;
	delete _nodeFlechaLimit;
	delete _balon;
}

Flecha& Flecha::operator= (const Flecha &obj){
	delete _nodePadreFlecha;
	delete _nodeFlecha;
	delete _nodeFlechaLimit;
	delete _balon;
	_nodePadreFlecha = obj._nodePadreFlecha;
	_nodeFlecha = obj._nodeFlecha;
	_nodeFlechaLimit = obj._nodeFlechaLimit;
	_balon = obj._balon;
	return *this;
}

SceneNode* Flecha::getNodePadreFlecha()const {
	return _nodePadreFlecha;
}

SceneNode* Flecha::getNodeFlecha()const{
	return _nodeFlecha;
}

SceneNode* Flecha::getNodeFlechaLimit()const {
	return _nodeFlechaLimit;
}

Balon* Flecha::getBalon()const {
	return _balon;
}

void Flecha::moveFront(){
	if(_nodePadreFlecha->getPosition().x < INICIO_X + DISTANCIA_LIMITE){
		_nodePadreFlecha->translate(0.1, 0, 0);
	}
}

void Flecha::moveBack(){
	if(_nodePadreFlecha->getPosition().x > INICIO_X - DISTANCIA_LIMITE){
		_nodePadreFlecha->translate(-0.1, 0, 0);
	}
}

void Flecha::moveLeft(){
	if(_nodePadreFlecha->getPosition().z > INICIO_Z - DISTANCIA_LIMITE){
		_nodePadreFlecha->translate(0, 0, -0.1);
	}
}

void Flecha::moveRight(){
	if(_nodePadreFlecha->getPosition().z < INICIO_Z + DISTANCIA_LIMITE){
		_nodePadreFlecha->translate(0, 0, 0.1);
	}
}

void Flecha::turnUp(){
	if(_nodeFlechaLimit->_getDerivedPosition().y < 1.5)
		_nodeFlecha->roll(Degree(1));
}

void Flecha::turnDown(){
	if(_nodeFlechaLimit->_getDerivedPosition().y > _nodePadreFlecha->_getDerivedPosition().y)
		_nodeFlecha->roll(Degree(-1));
}
void Flecha::turnLeft(){
	_nodePadreFlecha->yaw(Degree(1));
}
void Flecha::turnRight(){
	_nodePadreFlecha->yaw(Degree(-1));
}
void Flecha::escalar(double escala){
	_nodeFlecha->setScale(escala, 1, escala);
}

void Flecha::disparar(btVector3 impulso) {
	_balon->disparar(impulso);
}

btVector3 Flecha::calcularImpulso() {
	//calculamos la direccion del impulso
	double impulseX = 5 * (_nodeFlechaLimit->_getDerivedPosition().x-_nodePadreFlecha->getPosition().x);
	double impulseY = 5 * (_nodeFlechaLimit->_getDerivedPosition().y-_nodePadreFlecha->getPosition().y);
	double impulseZ = 5 * (_nodeFlechaLimit->_getDerivedPosition().z-_nodePadreFlecha->getPosition().z);

	return btVector3(impulseX, impulseY, impulseZ);
}

int Flecha::getDisparos() {
	return _numDisparos;
}

void Flecha::decrementarDisparos() {
	_numDisparos -= 1;
}
void Flecha::ocultar() {
	_nodeFlecha->setVisible(false);
}

void Flecha::mostrar() {
	_nodeFlecha->setVisible(true);
}

int Flecha::getAngulo(){
	calcularAngulo();

	return _angulo;
}

void Flecha::calcularAngulo(){
	double hipotenusa = sqrt(pow(_nodePadreFlecha->_getDerivedPosition().x-_nodeFlechaLimit->_getDerivedPosition().x,2) + pow(_nodePadreFlecha->_getDerivedPosition().y-_nodeFlechaLimit->_getDerivedPosition().y,2) + pow(_nodePadreFlecha->_getDerivedPosition().z-_nodeFlechaLimit->_getDerivedPosition().z,2));
	double cateto = sqrt(pow(_nodePadreFlecha->_getDerivedPosition().x-_nodeFlechaLimit->_getDerivedPosition().x,2) + pow(_nodePadreFlecha->_getDerivedPosition().z-_nodeFlechaLimit->_getDerivedPosition().z,2));

	double cosAngulo = cateto/hipotenusa;

	_angulo = acos(cosAngulo)*(180/3.1415);
}
