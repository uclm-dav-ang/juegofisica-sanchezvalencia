#define UNUSED_VARIABLE(x) (void)x

#include "GameManager.h"
#include "IntroState.h"
#include "PlayState.h"
#include "PlayStateNivel2.h"
#include "PlayStateNivel3.h"
#include "PauseState.h"
#include "LoseState.h"
#include "RecordsState.h"
#include "CreditosState.h"

#include <iostream>

using namespace std;

int main () {

  GameManager* game = new GameManager();
  IntroState* introState = new IntroState();
  PlayState* playState = new PlayState();
  PlayStateNivel2* playStateNivel2 = new PlayStateNivel2();
  PlayStateNivel3* playStateNivel3 = new PlayStateNivel3();
  PauseState* pauseState = new PauseState();
  LoseState* loseState = new LoseState();
  RecordsState* recordsState = new RecordsState();
  CreditosState* creditosState = new CreditosState();

  UNUSED_VARIABLE(introState);
  UNUSED_VARIABLE(playState);
  UNUSED_VARIABLE(playStateNivel2);
  UNUSED_VARIABLE(playStateNivel3);
  UNUSED_VARIABLE(pauseState);
  UNUSED_VARIABLE(loseState);
  UNUSED_VARIABLE(recordsState);
  UNUSED_VARIABLE(creditosState);

  try
    {
      // Inicializa el juego y transición al primer estado.
      game->start(IntroState::getSingletonPtr());
    }
  catch (Ogre::Exception& e)
    {
      std::cerr << "Excepción detectada: " << e.getFullDescription();
    }
  
  delete game;
  
  return 0;
}
