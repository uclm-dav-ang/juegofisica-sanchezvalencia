#include <Balon.h>
#include <OgreSceneNode.h>
#include <cmath>

Balon::Balon(SceneNode* nodeBalon, btCollisionShape* ballShape, btRigidBody* ballRigidBody) {
	_nodeBalon = nodeBalon;
	_ballShape = ballShape;
	_ballRigidBody = ballRigidBody;
	_gameManager = GameManager::getSingletonPtr();
	_puntos = 0;
}

Balon::Balon(const Balon &obj){
	_nodeBalon = obj.getNode();
	_ballShape = obj.getBallShape();
	_ballRigidBody = obj.getBallRigidBody();
}

Balon::~Balon() {
	delete _nodeBalon;
	delete _ballShape;
	delete _ballRigidBody;
}

Balon& Balon::operator= (const Balon &obj){
	delete _nodeBalon;
	delete _ballShape;
	delete _ballRigidBody;
	_nodeBalon = obj._nodeBalon;
	_ballShape = obj._ballShape;
	_ballRigidBody = obj._ballRigidBody;
	return *this;
}

SceneNode* Balon::getNode()const {
	return _nodeBalon;
}

btCollisionShape* Balon::getBallShape()const {
	return _ballShape;
}

btRigidBody* Balon::getBallRigidBody()const {
	return _ballRigidBody;
}

void Balon::disparar(btVector3 impulso){
	_ballRigidBody->applyCentralImpulse(impulso);
}

void Balon::pararBalon(){

}

void Balon::incrementarPuntos(){
	_puntos += 10;
}
